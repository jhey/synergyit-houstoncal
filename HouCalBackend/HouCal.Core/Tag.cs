﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HouCal.Core
{
    public class Tag
    {
        private DateTime created;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid TagId { get; set; }

        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Created
        {
            get { return created; }
            set { created = value; }
        }

        public DateTime? LastUpdated { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public virtual List<Event> TaggedEvents { get; set; }

        public Tag()
        {
            created = DateTime.UtcNow;
        }
    }
}
