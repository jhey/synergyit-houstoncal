﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace HouCal.Core
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId { get; set; }

        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Created { get; set; }

        public DateTime? LastUpdated { get; set; }

        [DefaultValue(false)]
        public bool IsPublished { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        public DateTime? Expires { get; set; }

        [StringLength(40)]
        public string FirstName { get; set; }

        [StringLength(40)]
        [Required]
        public string LastName { get; set; }

        [StringLength(2000)]
        public string Biography { get; set; }

        [StringLength(50)]
        [Required]
        public string Email { get; set; }

        [DefaultValue(false)]
        public bool EmailVerified { get; set; }

        [StringLength(10)]
        public string Mobile { get; set; }

        [DefaultValue(false)]
        public bool MobileVerified { get; set; }

        [DefaultValue(false)]
        public bool BrowserVerified { get; set; }

        [StringLength(100)]
        public string WebsiteURL { get; set; }

        [StringLength(16)]
        public string Twitter { get; set; }

        [StringLength(100)]
        public string LinkedInURL { get; set; }

        [StringLength(60)]
        public string Address1 { get; set; }

        [StringLength(40)]
        public string Address2 { get; set; }

        [StringLength(40)]
        public string City { get; set; }

        [StringLength(40)]
        public string State { get; set; }

        [StringLength(10)]
        public string PostalCode { get; set; }

        public DbGeography Geocode { get; set; }

        public virtual List<Event> CreatedEvents { get; set; }
    }
}
