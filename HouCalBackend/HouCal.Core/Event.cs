using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace HouCal.Core
{
    public class Event
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid EventId { get; set; }

        [Required]
        [ForeignKey("CreatedById")]
        [InverseProperty("CreatedEvents")]
        public User CreatedBy { get; set; }

        public Guid CreatedById { get; set; }

        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Created { get; set; }

        public User LastUpdatedBy { get; set; }

        public Guid? LastUpdatedById { get; set; }

        public DateTime? LastUpdated { get; set; }

        [DefaultValue(false)]
        public bool IsPublished { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        public DateTime? Expires { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(60)]
        public string Address1 { get; set; }

        [StringLength(40)]
        public string Address2 { get; set; }

        [StringLength(40)]
        public string City { get; set; }

        [StringLength(40)]
        public string State { get; set; }

        [StringLength(10)]
        public string PostalCode { get; set; }

        public DbGeography Geocode { get; set; }

        [StringLength(2000)]
        public string Description { get; set; }

        public DateTime Begins { get; set; }

        public DateTime Ends { get; set; }

        public virtual List<Tag> EventTags { get; set; }
    }
}
