namespace HouCal.SQLDataModel.Migrations
{
    using Core;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<HouCal.SQLDataModel.HouCalContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HouCal.SQLDataModel.HouCalContext context)
        {
            context.Tags.AddOrUpdate(
                t => t.Name,
                // General
                new Tag { Name = "Houston City Council Session" },
                new Tag { Name = "Neighborhood Association" },
                new Tag { Name = "Townhall Meeting" },
                new Tag { Name = "Houston District A" },
                new Tag { Name = "Houston District B" },
                new Tag { Name = "Houston District C" },
                new Tag { Name = "Houston District D" },
                new Tag { Name = "Houston District E" },
                new Tag { Name = "Houston District F" },
                new Tag { Name = "Houston District G" },
                new Tag { Name = "Houston District H" },
                new Tag { Name = "Houston District I" },
                new Tag { Name = "Houston District J" },
                new Tag { Name = "Houston District K" },
                new Tag { Name = "Houston At-Large 1" },
                new Tag { Name = "Houston At-Large 2" },
                new Tag { Name = "Houston At-Large 3" },
                new Tag { Name = "Houston At-Large 4" },
                new Tag { Name = "Houston At-Large 5" },
                new Tag { Name = "Houston Fire Department" },
                new Tag { Name = "Houston Police Department" },
                // Houston Superneighborhoods http://www.houstontx.gov/superneighborhoods/recognized.html
                new Tag { Name = "Acres Home" },
                new Tag { Name = "Addicks Park Ten" },
                new Tag { Name = "Afton Oaks / River Oaks" },
                new Tag { Name = "Alief" },
                new Tag { Name = "Astrodome Area" },
                new Tag { Name = "Braeburn" },
                new Tag { Name = "Braeswood" },
                new Tag { Name = "Brays Oaks" },
                new Tag { Name = "Briar Forest" },
                new Tag { Name = "Carverdale" },
                new Tag { Name = "Central Northwest" },
                new Tag { Name = "Central Southwest" },
                new Tag { Name = "Clear Lake" },
                new Tag { Name = "Clinton Park / Tri-Community" },
                new Tag { Name = "Denver Harbor / Port Houston" },
                new Tag { Name = "Downtown" },
                new Tag { Name = "East Houston" },
                new Tag { Name = "East Little York / Homestead" },
                new Tag { Name = "Eastex - Jensen" },
                new Tag { Name = "Edgebrook" },
                new Tag { Name = "El Dorado / West Oaks" },
                new Tag { Name = "Fairbanks / Northwest Crossing" },
                new Tag { Name = "Fondren Gardens" },
                new Tag { Name = "Fort Bend / Houston" },
                new Tag { Name = "Fourth Ward" },
                new Tag { Name = "Golfcrest / Bellfort / Reveille" },
                new Tag { Name = "Greater Eastwood" },
                new Tag { Name = "Greater Fifth Ward" },
                new Tag { Name = "Greater Greenspoint" },
                new Tag { Name = "Greater Heights" },
                new Tag { Name = "Greater Hobby Area" },
                new Tag { Name = "Greater Inwood" },
                new Tag { Name = "Greater OST / South Union" },
                new Tag { Name = "Greater Third Ward" },
                new Tag { Name = "Greater Uptown" },
                new Tag { Name = "Greenway / Upper Kirby" },
                new Tag { Name = "Gulfgate Riverview / Pine Valley" },
                new Tag { Name = "Gulfton" },
                new Tag { Name = "Harrisburg / Manchester" },
                new Tag { Name = "Hidden Valley" },
                new Tag { Name = "Hunterwood" },
                new Tag { Name = "IAH Airport" },
                new Tag { Name = "Independence Heights" },
                new Tag { Name = "Kashmere Gardens" },
                new Tag { Name = "Kingwood" },
                new Tag { Name = "Lake Houston" },
                new Tag { Name = "Langwood" },
                new Tag { Name = "Lawndale / Wayside" },
                new Tag { Name = "Lazybrook / Timbergrove" },
                new Tag { Name = "MacGregor" },
                new Tag { Name = "Magnolia Park" },
                new Tag { Name = "Meadowbrook / Allendale" },
                new Tag { Name = "Medical Center" },
                new Tag { Name = "Memorial" },
                new Tag { Name = "Meyerland" },
                new Tag { Name = "Mid-West" },
                new Tag { Name = "Midtown" },
                new Tag { Name = "Minnetex" },
                new Tag { Name = "Museum Park" },
                new Tag { Name = "Neartown / Montrose" },
                new Tag { Name = "Northshore" },
                new Tag { Name = "Northside / Northline" },
                new Tag { Name = "Northside Village" },
                new Tag { Name = "Park Place" },
                new Tag { Name = "Pecan Park" },
                new Tag { Name = "Pleasantville Area" },
                new Tag { Name = "Second Ward" },
                new Tag { Name = "Settegast" },
                new Tag { Name = "Sharpstown" },
                new Tag { Name = "South Acres / Crestmont Park" },
                new Tag { Name = "South Belt / Ellington" },
                new Tag { Name = "South Main" },
                new Tag { Name = "South Park" },
                new Tag { Name = "Spring Branch Central" },
                new Tag { Name = "Spring Branch East" },
                new Tag { Name = "Spring Branch North" },
                new Tag { Name = "Spring Branch West" },
                new Tag { Name = "Sunnyside" },
                new Tag { Name = "Trinity / Houston Gardens" },
                new Tag { Name = "University Place" },
                new Tag { Name = "Washington Avenue Coalition / Memorial Park" },
                new Tag { Name = "Westbranch" },
                new Tag { Name = "Westbury" },
                new Tag { Name = "Westchase" },
                new Tag { Name = "Westwood" },
                new Tag { Name = "Willow Meadows / Willowbend" },
                new Tag { Name = "Willowbrook" }
            );

            SaveChanges(context);
        }

        /// <summary>
        /// Wrapper for SaveChanges adding the Validation Messages to the generated exception
        /// </summary>
        /// <param name="context">The context.</param>
        private void SaveChanges(DbContext context)
        {
            // Source: http://stackoverflow.com/questions/10219864/ef-code-first-how-do-i-see-entityvalidationerrors-property-from-the-nuget-pac

            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                ); // Add the original exception as the innerException
            }
        }

    }
}
