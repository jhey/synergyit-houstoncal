namespace HouCal.SQLDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        EventId = c.Guid(nullable: false, identity: true),
                        CreatedById = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        LastUpdatedById = c.Guid(),
                        LastUpdated = c.DateTime(),
                        IsPublished = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Expires = c.DateTime(),
                        Name = c.String(maxLength: 100),
                        Address1 = c.String(maxLength: 60),
                        Address2 = c.String(maxLength: 40),
                        City = c.String(maxLength: 40),
                        State = c.String(maxLength: 40),
                        PostalCode = c.String(maxLength: 10),
                        Geocode = c.Geography(),
                        Description = c.String(maxLength: 2000),
                        Begins = c.DateTime(nullable: false),
                        Ends = c.DateTime(nullable: false),
                        LastUpdatedBy_UserId = c.Guid(),
                    })
                .PrimaryKey(t => t.EventId)
                .ForeignKey("dbo.Users", t => t.CreatedById, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.LastUpdatedBy_UserId)
                .Index(t => t.CreatedById)
                .Index(t => t.LastUpdatedBy_UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Guid(nullable: false, identity: true),
                        Created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        LastUpdated = c.DateTime(),
                        IsPublished = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Expires = c.DateTime(),
                        FirstName = c.String(maxLength: 40),
                        LastName = c.String(nullable: false, maxLength: 40),
                        Biography = c.String(maxLength: 2000),
                        Email = c.String(nullable: false, maxLength: 50),
                        EmailVerified = c.Boolean(nullable: false),
                        Mobile = c.String(maxLength: 10),
                        MobileVerified = c.Boolean(nullable: false),
                        BrowserVerified = c.Boolean(nullable: false),
                        WebsiteURL = c.String(maxLength: 100),
                        Twitter = c.String(maxLength: 16),
                        LinkedInURL = c.String(maxLength: 100),
                        Address1 = c.String(maxLength: 60),
                        Address2 = c.String(maxLength: 40),
                        City = c.String(maxLength: 40),
                        State = c.String(maxLength: 40),
                        PostalCode = c.String(maxLength: 10),
                        Geocode = c.Geography(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagId = c.Guid(nullable: false, identity: true),
                        Created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        LastUpdated = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.TagId);
            
            CreateTable(
                "dbo.TagEvents",
                c => new
                    {
                        Tag_TagId = c.Guid(nullable: false),
                        Event_EventId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagId, t.Event_EventId })
                .ForeignKey("dbo.Tags", t => t.Tag_TagId, cascadeDelete: true)
                .ForeignKey("dbo.Events", t => t.Event_EventId, cascadeDelete: true)
                .Index(t => t.Tag_TagId)
                .Index(t => t.Event_EventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Events", "LastUpdatedBy_UserId", "dbo.Users");
            DropForeignKey("dbo.TagEvents", "Event_EventId", "dbo.Events");
            DropForeignKey("dbo.TagEvents", "Tag_TagId", "dbo.Tags");
            DropForeignKey("dbo.Events", "CreatedById", "dbo.Users");
            DropIndex("dbo.TagEvents", new[] { "Event_EventId" });
            DropIndex("dbo.TagEvents", new[] { "Tag_TagId" });
            DropIndex("dbo.Events", new[] { "LastUpdatedBy_UserId" });
            DropIndex("dbo.Events", new[] { "CreatedById" });
            DropTable("dbo.TagEvents");
            DropTable("dbo.Tags");
            DropTable("dbo.Users");
            DropTable("dbo.Events");
        }
    }
}
