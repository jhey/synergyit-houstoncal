﻿using HouCal.Core;
using System.Data.Entity;

namespace HouCal.SQLDataModel
{
    public class HouCalContext : DbContext
    {
        public DbSet<Event> Events { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public HouCalContext()
            : base("HouCal")
        {
        }
    }
}
